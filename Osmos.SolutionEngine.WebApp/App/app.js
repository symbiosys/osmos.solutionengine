﻿angular.module('app', ['ngRoute', 'ui.bootstrap']);

angular.module('app').config(['$routeProvider',
    function ($routeProvider) {

        $routeProvider
            .when('/',
                {
                    templateUrl: 'App/modules/home/home.html',
                    controller: 'homeCtrl as vm',
                    name: 'Home'
                })
            .otherwise({ redirectTo: '/' });

        appHub.start();
    }]);