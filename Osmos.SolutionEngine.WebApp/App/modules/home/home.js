﻿(function () {
    angular.module('app').controller('homeCtrl',
        ['$scope', homeCtrl]);

    function homeCtrl($scope) {

        var vm = this;

        vm.logs = [];
        vm.solutionRoot = null;

        vm.solutionPath =
            'C:\\Users\\DaFrisky\\Documents\\visual studio 2013\\Projects\\Osmos.SolutionEngine\\' +
            'TestSolutions\\TestProject.Working\\TestProject.Working.sln';

        vm.loadSolution = function () {
            vm.logs = [];
            appHub.invoke('loadSolution', vm.solutionPath);
        }

        vm.buildProject = function (projectId) {
            vm.logs = [];
            appHub.invoke('buildProject', projectId);
        }

        vm.buildSolution = function () {
            vm.logs = [];
            appHub.invoke('buildSolution');
        }

        vm.exploreSolution = function () {
            vm.logs = [];
            appHub.invoke('exploreSolution');
        }

        appHub.on('logMessage', function (message) {
            console.log(message);
            vm.logs.push({message : message});
            $scope.$apply();
        });

        appHub.on('setSolution', function (solution) {
            console.log(solution);
            vm.solution = angular.copy(solution);
            $scope.$apply();
        });

        appHub.on('SetSolutionExplorer', function (root) {
            console.log('setSolutionExplorer');
            vm.solutionRoot = angular.copy(root);
            console.log(vm.solutionRoot);
            //vm.logs.push({ message: message });
            $scope.$apply();
        });
    }

})();