﻿(function () {

    var connection = $.hubConnection();
    //connection.logging = true;
    var appHubProxy = connection.createHubProxy('appHub');

    appHubProxy.on('hello', function () {
    });

    window.appHub = {
        started: false,
        start: function () {
            var self = this;
            return connection.start()
                .done(function () {
                    self.started = true;
                    console.log('You are now online.');
                })
                .fail(function () {
                    self.started = false;
                    console.error('Can not start SignalR !');
                });
        },

        on: function (functionName, callback) {
            appHubProxy.on(functionName, callback);
        },
        off: function (functionName) {
            appHubProxy.off(functionName);
        },

        invoke: function () {
            var self = this;
            if (arguments.length < 1) {
                console.log('the Invoke method need at least one argument !');
                return;
            }

            if (self.started) {
                appHubProxy.invoke.apply(appHubProxy, arguments);
            }
            else {
                console.error('SignalR is not started.');
            }
        }
    };
}());