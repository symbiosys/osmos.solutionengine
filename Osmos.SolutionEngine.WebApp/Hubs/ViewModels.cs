﻿using Osmos.SolutionEngine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Osmos.SolutionEngine.WebApp.Hubs
{
    class ProjectReferenceViewModel
    {
        public string name { get; set; }
    }

    class BuilderViewModel
    {
        public string status { get; set; }
        public int errorCount { get; set; }
        public int warningCount { get; set; }
    }

    class ProjectViewModel
    {

        public ProjectViewModel(IProject project)
        {
            id = project.ProjectId;
            name = project.ProjectName;
            type = project.ProjectType.Name;
            builder = new BuilderViewModel
            {
                status = project.Builder.BuildStatus.ToString(),
                warningCount = project.Builder.WarningsCount,
                errorCount = project.Builder.ErrorsCount
            };
            references = project.ProjectReferences.Select(r => new ProjectReferenceViewModel
            {
                name = r.Name
            }).ToArray();
        }

        public Guid id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public BuilderViewModel builder { get; set; }
        public ProjectReferenceViewModel[] references { get; set; }
    }

    class SolutionViewModel
    {

        public SolutionViewModel(Solution solution)
        {
            name = solution.SolutionName;
            if (solution.Projects != null)
            {
                projects = solution.Projects.Select(p => new ProjectViewModel(p));
            }
        }

        public string name { get; set; }
        public IEnumerable<ProjectViewModel> projects { get; set; }
    }
}