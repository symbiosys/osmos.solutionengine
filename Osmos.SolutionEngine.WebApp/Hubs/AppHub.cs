﻿using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Osmos.SolutionEngine.WebApp.Hubs
{
    public class AppHub : Hub
    {
        static List<Session> _sessions = new List<Session>();

        public override Task OnConnected()
        {
            Clients.Caller.LogMessage("OnConnected");
            Session.SessionConnect(_sessions, Context.ConnectionId);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            Clients.Caller.LogMessage("OnDisconnected");
            Session.SessionDisconnect(_sessions, Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        public void LoadSolution(string solutionFilePath)
        {
            var solution = Solution.Load(new SolutionOptions
            {
                SolutionFilePath = solutionFilePath,
                LoggingService = new LoggingService(Clients.Caller)
            });

            Session.SetSessionSolution(_sessions, Context.ConnectionId, solution);

            Clients.Caller.SetSolution(new SolutionViewModel(solution));
        }

        public void ExploreSolution()
        {
            var solution = Session.GetSessionSolution(_sessions, Context.ConnectionId);
            if (solution != null)
            {
                //var tree = solution.SolutionExplorer.GetRoot();
                //Clients.Caller.SetSolutionExplorer(CamelCaseObject(tree));
            }
        }

        public async Task BuildProject(Guid projectId)
        {
            var solution = Session.GetSessionSolution(_sessions, Context.ConnectionId);
            if (solution != null && solution.Projects != null)
            {
                var project = solution.Projects.FirstOrDefault(p => p.ProjectId == projectId);
                if (project != null) await project.Builder.BuildAsync();
            }
        }

        public async Task BuildSolution()
        {
            var solution = Session.GetSessionSolution(_sessions, Context.ConnectionId);
            if (solution != null) await solution.Builder.BuildAsync();
        }

        private static object CamelCaseObject(object data)
        {
            string json = JsonConvert.SerializeObject(
                    data,
                    Formatting.None, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() }
                    );
            return JsonConvert.DeserializeObject(json);
        }
    }
}