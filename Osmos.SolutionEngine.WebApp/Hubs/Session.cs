﻿using System.Collections.Generic;
using System.Linq;

namespace Osmos.SolutionEngine.WebApp.Hubs
{
    class Session
    {
        public string connectionId { get; set; }
        public Solution solution { get; set; }
        public static Solution GetSessionSolution(List<Session> sessions, string connectionId)
        {
            Session session;
            lock (sessions)
            {
                session = sessions.FirstOrDefault(s => s.connectionId == connectionId);
            }
            if (session != null && session.solution != null)
            {
                return session.solution;
            }
            return null;
        }

        public static void SetSessionSolution(List<Session> sessions, string connectionId, Solution solution)
        {
            lock (sessions)
            {
                var session = sessions.FirstOrDefault(s => s.connectionId == connectionId);
                if (session != null) session.solution = solution;
            }
        }

        public static void SessionDisconnect(List<Session> sessions, string connectionId)
        {
            lock (sessions)
            {
                var session = sessions.FirstOrDefault(s => s.connectionId == connectionId);
                if (session != null) sessions.Remove(session);
            }
        }

        public static void SessionConnect(List<Session> sessions, string connectionId)
        {
            lock (sessions)
            {
                sessions.Add(new Session
                {
                    connectionId = connectionId,
                    solution = null
                });
            }
        }
    }
}