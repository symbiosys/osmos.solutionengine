﻿using Osmos.CmdExecuter;

namespace Osmos.SolutionEngine.WebApp.Hubs
{
    class LoggingService : ILoggingService
    {
        dynamic _client;
        public LoggingService(dynamic client)
        {
            _client = client;
        }
        public void Log(string message)
        {
            _client.LogMessage(message);
        }
    }
}