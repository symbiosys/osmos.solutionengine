﻿using Osmos.CmdExecuter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Osmos.SolutionEngine.ConsoleApp
{
    class LoggingService : ILoggingService
    {
        public void Log(string message)
        {
            System.Console.WriteLine(message);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            string rootPath = Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName).FullName;
            string solutionFilePath = Path.Combine(rootPath, @"TestSolutions\TestProject.Working\TestProject.Working.sln");

            var solution = Solution.Load(new SolutionOptions
            {
                SolutionFilePath = solutionFilePath,
                LoggingService = new LoggingService()
            });

            string compileFilePath = solution.Projects[0].ProjectExplorer.CompileFilePaths[0];
            string projectFileFolderPath = Path.GetDirectoryName(solution.Projects[0].ProjectFilePath);
            string compileFileFullPath = Path.Combine(projectFileFolderPath, compileFilePath);

            //var task = Task.Run(async () => await solution.Projects[0].Builder.BuildAsync());
            //task.Wait();

            //string solutionFolderPath = Path.GetDirectoryName(solutionFilePath);
            //var root = FolderItem.GetRoot(solutionFolderPath);
        }
    }
}
