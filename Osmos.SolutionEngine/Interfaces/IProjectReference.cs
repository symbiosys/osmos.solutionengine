﻿using System;

namespace Osmos.SolutionEngine.Interfaces
{
    public interface IProjectReference
    {
        string Name { get; }
        Version Version { get; }
    }
}
