﻿using System;

namespace Osmos.SolutionEngine.Interfaces
{
    public interface IProjectType
    {
        Guid Id { get; }
        string Name { get; }
    }
}
