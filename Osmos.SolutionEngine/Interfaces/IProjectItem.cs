﻿using System;

namespace Osmos.SolutionEngine.Interfaces
{
    public interface IProjectItem
    {
        string Name { get; }
        string FullPath { get; }
        DateTime CreatedDate { get; }
        DateTime UpdatedDate { get; }
        bool IsFolder { get; }
        bool IsIncluded { get; }
        long? Size { get; }

        IProjectItem Parent { get; }
        IProjectItem[] Children { get; }

        IProjectItem Search(string fullPath);
    }
}
