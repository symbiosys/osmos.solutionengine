﻿
using Osmos.ProjectBuilder;
using System;
namespace Osmos.SolutionEngine.Interfaces
{
    public interface IProject
    {
        Guid ProjectId { get; }
        string ProjectFilePath { get; }
        string ProjectName { get; }
        IProjectType ProjectType { get; }
        Builder Builder { get; }
        IProjectReference[] ProjectReferences { get; }
        IProjectExplorer ProjectExplorer { get; }
    }
}
