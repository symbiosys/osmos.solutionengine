﻿
namespace Osmos.SolutionEngine.Interfaces
{
    public interface IProjectExplorer
    {
        void Load();
        string ProjectFilePath { get; }
        string[] CompileFilePaths { get; }
        string[] ContentFilePaths { get; }
        IProjectItem Root { get; }
    }
}
