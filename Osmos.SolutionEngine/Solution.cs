﻿using Osmos.CmdExecuter;
using Osmos.ProjectBuilder;
using Osmos.SolutionEngine.Interfaces;
using Osmos.SolutionEngine.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Osmos.SolutionEngine
{
    public class SolutionOptions {
        public string SolutionFilePath { get; set; }
        public string MsBuildPath { get; set; }
        public ILoggingService LoggingService { get; set; }
    }

    public class Solution
    {
        public static Solution Load(SolutionOptions options)
        {
            if (options.LoggingService != null) options.LoggingService.Log("Started Loading the solution");

            if (options == null)
                throw new ArgumentNullException("SolutionOptions object can not be null.");

            if (!File.Exists(options.SolutionFilePath))
                throw new FileNotFoundException(string.Format("Can not find the solution file \"{0}\"", options.SolutionFilePath));

            var solution = new Solution();
            solution._solutionFilePath = options.SolutionFilePath;
            solution._solutionName = Path.GetFileNameWithoutExtension(options.SolutionFilePath);
            solution._solutionDirectoryPath = Path.GetDirectoryName(options.SolutionFilePath);
            if (options.MsBuildPath != null) solution._msbuildPath = options.MsBuildPath;
            solution._loggingService = options.LoggingService;

            solution._LoadProjects();

            if (options.LoggingService != null) options.LoggingService.Log("Finished Loading the solution");

            var builderOptions = new BuilderOptions
            {
                FilePath = solution._solutionFilePath,
                MsBuildPath = solution._msbuildPath,
                LoggingService = solution._loggingService
            };
            solution._builder = Builder.Create(builderOptions);

            return solution;
        }

        public string SolutionFilePath
        {
            get
            {
                return _solutionFilePath;
            }
        }
        public string SolutionName
        {
            get
            {
                return _solutionName;
            }
        }
        public IProject[] Projects
        {
            get
            {
                return _projects.ToArray();
            }
        }
        public Builder Builder { get { return _builder; } }



        string _solutionFilePath;
        string _solutionName;
        string _solutionDirectoryPath;
        string _msbuildPath;
        List<IProject> _projects;
        ILoggingService _loggingService;
        Builder _builder;

        private void _LoadProjects() {
            var Content = File.ReadAllText(_solutionFilePath); // TODO: asynchronize
            var projReg = new Regex(
                //"Project\\(\"\\{[\\w-]*\\}\"\\) = \"([\\w _]*.*)\", \"(.*\\.(cs|vcx|vb)proj)\""
                "Project\\(\"\\{[\\w-]*\\}\"\\) = \"([\\w _]*.*)\", \"(.*\\.csproj)\""
                , RegexOptions.Compiled);
            var projMatches = projReg.Matches(Content);

            foreach (Match projMatch in projMatches)
            {
                string solutionProjectDefinition = projMatch.Value;

                var project = Project.Load(new ProjectOptions {
                    SolutionProjectDefinition = solutionProjectDefinition,
                    SolutionDirectoryPath = _solutionDirectoryPath,
                    MsBuildPath = _msbuildPath,
                    LoggingService = _loggingService
                });
                if (_projects == null) {
                    _projects = new List<IProject>();
                }
                _projects.Add(project);
            }
        }
    }
}
