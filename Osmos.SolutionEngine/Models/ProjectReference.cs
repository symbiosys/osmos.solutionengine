﻿using Osmos.SolutionEngine.Interfaces;
using System;

namespace Osmos.SolutionEngine.Models
{
    class ProjectReference : IProjectReference
    {
        public string Name { get { return _name; } }
        public Version Version { get { return _version; } }
        public void SetVersion(Version version) {
            _version = version;
        }

        public ProjectReference(string name, Version version)
        {
            _name = name;
            _version = version;
        }

        string _name;
        Version _version;
    }
}
