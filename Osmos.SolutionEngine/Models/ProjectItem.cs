﻿using Osmos.SolutionEngine.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Osmos.SolutionEngine.Models
{
    class ProjectItem : IProjectItem
    {
        public string Name { get { return _name; } }
        public string FullPath { get { return _fullPath; } }
        public DateTime CreatedDate { get { return _createdDate; } }
        public DateTime UpdatedDate { get { return _updatedDate; } }
        public bool IsFolder { get { return _isFolder; } }
        public bool IsIncluded { get{ return _isIncluded; } }
        public long? Size { get { return _size; } }

        public IProjectItem Parent { get { return _parent; } }
        public IProjectItem[] Children { get { return _children.ToArray(); } }

        string _name, _fullPath;
        DateTime _createdDate, _updatedDate;
        bool _isFolder, _isIncluded = false;
        long? _size;
        List<IProjectItem> _children = new List<IProjectItem>();
        IProjectItem _parent = null;

        private void _SetFiles()
        {
            string[] filePaths = Directory.GetFiles(FullPath);
            foreach (string filePath in filePaths)
            {
                var projectItem = new ProjectItem
                {
                    _name = Path.GetFileName(filePath),
                    _fullPath = filePath,
                    _createdDate = File.GetCreationTime(filePath),
                    _updatedDate = File.GetLastWriteTime(filePath),
                    _isFolder = false,
                    _size = new FileInfo(filePath).Length,
                    _parent = this
                };
                _children.Add(projectItem);
            }
        }

        private static IProjectItem _DirectoryFromPath(string folderPath, IProjectItem parent = null)
        {
            var projectItem = new ProjectItem
            {
                _name = Directory.CreateDirectory(folderPath).Name,
                _fullPath = folderPath,
                _createdDate = Directory.GetCreationTime(folderPath),
                _updatedDate = Directory.GetLastWriteTime(folderPath),
                _isFolder = true,
                _size = null,
                _parent = parent
            };
            return projectItem;
        }

        private void _SetChildren()
        {
            string[] subdirectoryPaths = Directory.GetDirectories(FullPath);
            foreach (string subdirectoryPath in subdirectoryPaths)
            {
                var projectItem = ProjectItem._DirectoryFromPath(subdirectoryPath, this);
                (projectItem as ProjectItem)._SetChildren();
                _children.Add(projectItem);
            }

            _SetFiles();
        }

        public IProjectItem Search(string fullPath)
        {
            if (FullPath == fullPath) return this;
            foreach (var child in Children)
            {
                var projectItem = child.Search(fullPath);
                if (projectItem != null) return projectItem;
            }
            return null;
        }

        private void _SetIsIncluded(List<string> fileFullPaths)
        {
            if (fileFullPaths.Contains(_fullPath))
            {
                _isIncluded = true;
                var ascendant = Parent;
                while (ascendant != null)
                {
                    (ascendant as ProjectItem)._isIncluded = true;
                    ascendant = ascendant.Parent;
                }
            }
            else if (!IsFolder) {
                _isIncluded = false;
            }

            foreach (var child in _children)
            {
                (child as ProjectItem)._SetIsIncluded(fileFullPaths);
            }
        }

        public static IProjectItem GetRoot(IProjectExplorer projectExplorer)
        {
            string projectFileFolderPath = Path.GetDirectoryName(projectExplorer.ProjectFilePath);
            var projectItem = ProjectItem._DirectoryFromPath(projectFileFolderPath);

            (projectItem as ProjectItem)._SetChildren();

            var fileFullPaths = new List<string>();
            fileFullPaths.AddRange(projectExplorer.CompileFilePaths.Select(fp => Path.Combine(projectFileFolderPath, fp)));
            fileFullPaths.AddRange(projectExplorer.ContentFilePaths.Select(fp => Path.Combine(projectFileFolderPath, fp)));
            (projectItem as ProjectItem)._SetIsIncluded(fileFullPaths);

            return projectItem;
        }
    }
}
