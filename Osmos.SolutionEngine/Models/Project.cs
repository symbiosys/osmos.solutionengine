﻿using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using System;
using Osmos.ProjectBuilder;
using Osmos.CmdExecuter;
using System.Xml;
using System.Collections.Generic;
using Osmos.SolutionEngine.Interfaces;
using Osmos.SolutionEngine.Exceptions;

namespace Osmos.SolutionEngine.Models
{
    class ProjectOptions {
        public string SolutionProjectDefinition { get; set; }
        public string SolutionDirectoryPath { get; set; }
        public string MsBuildPath { get; set; }
        public ILoggingService LoggingService { get; set; }
    }

    class Project : IProject
    {
        public Project()
        {
            _projectId = Guid.NewGuid();
        }

        public static Project Load(ProjectOptions options)
        {
            if (options == null)
                throw new ArgumentNullException("ProjectOptions object can not be null.");
            if (options.SolutionProjectDefinition == null)
                throw new ArgumentException("SolutionProjectDefinition can not be null.");
            if (options.SolutionDirectoryPath == null)
                throw new ArgumentException("SolutionDirectoryPath can not be null.");

            var projProps = options.SolutionProjectDefinition.Split(',');
            if (projProps.Length != 2) throw new BadSlnFileException();

            var guidReg = new Regex(@"\b[A-F0-9]{8}(?:-[A-F0-9]{4}){3}-[A-F0-9]{12}\b"
                , RegexOptions.Compiled);
            var guidMatches = guidReg.Matches(projProps[0]);
            if (guidMatches.Count != 1) throw new BadSlnFileException();
            string guidStr = guidMatches[0].Value;

            var projectType = ProjetTypes.All.FirstOrDefault(pt => pt.Id == new Guid(guidStr));
            if (projectType == null) throw new BadProjectIdException(guidStr);

            var projNameItems = projProps[0].Split('=');
            if (projNameItems.Length != 2 && projNameItems[1].Length == 0) throw new BadSlnFileException();

            string projectName = Regex.Replace(projNameItems[1].Substring(1), "\"", "");

            string projectFilePath = Path.Combine(options.SolutionDirectoryPath, Regex.Replace(projProps[1].Substring(1), "\"", ""));
            if (!File.Exists(projectFilePath))
                throw new FileNotFoundException(string.Format("Can not find the project file name at \"{0}\"", projectFilePath));
            projectFilePath = Path.GetFullPath(projectFilePath);

            var project = new Project();
            project._projectName = projectName;
            project._projectFilePath = projectFilePath;
            project._projectType = projectType;
            project._msbuildPath = options.MsBuildPath;
            project._loggingService = options.LoggingService;

            var builderOptions = new BuilderOptions
            {
                FilePath = project._projectFilePath,
                MsBuildPath = project._msbuildPath,
                LoggingService = project._loggingService
            };
            project._builder = Builder.Create(builderOptions);

            project._SetReferencesFromCsProj();
            project._UpdateReferencesFromPackageConfig();

            project._projectExplorer = new ProjectExplorer(project._projectFilePath);

            return project;
        }

        public Guid ProjectId { get { return _projectId; } }
        public string ProjectFilePath
        {
            get
            {
                return _projectFilePath;
            }
        }
        public string ProjectName
        {
            get
            {
                return _projectName;
            }
        }
        public IProjectType ProjectType { get { return _projectType; } }
        public Builder Builder { get { return _builder; } }
        public IProjectReference[] ProjectReferences { get { return _projectReferences; } }
        public IProjectExplorer ProjectExplorer { get { return _projectExplorer; } }

        Guid _projectId;
        string _projectFilePath;
        string _projectName;
        string _msbuildPath;
        IProjectType _projectType;
        Builder _builder;
        ILoggingService _loggingService;
        IProjectReference[] _projectReferences;
        IProjectExplorer _projectExplorer;

        private void _SetReferencesFromCsProj()
        {
            var csProjXmlDoc = new XmlDocument();
            csProjXmlDoc.Load(_projectFilePath);

            var projectReferencesList = new List<ProjectReference>();
            var referenceTags = csProjXmlDoc.GetElementsByTagName("Reference").Cast<XmlNode>().ToList();
            foreach (var referenceTag in referenceTags)
            {
                var attributes = referenceTag.Attributes.Cast<XmlAttribute>().ToList();
                var includeAttribute = attributes.FirstOrDefault(a => a.Name.ToLower() == "include");
                if (includeAttribute != null)
                {
                    string value = includeAttribute.Value;
                    var values = value.Split(',');
                    if (values.Any())
                    {
                        string name = values[0];
                        Version version = null;

                        if (values.Length > 1)
                        {
                            var versionValue = values[1].Split('=');
                            if (versionValue.Length > 1 && Regex.Replace(versionValue[0].ToLower(), @"\s+", "") == "version")
                            {
                                version = new Version(versionValue[1]);
                            }
                        }
                        var reference = new ProjectReference(name, version);

                        projectReferencesList.Add(reference);
                    }
                }
            }
            _projectReferences = projectReferencesList.ToArray();
        }

        private void _UpdateReferencesFromPackageConfig()
        {
            string packageConfigPath = Path.Combine(Path.GetDirectoryName(_projectFilePath), "packages.config");
            var packageConfigXmlDoc = new XmlDocument();
            packageConfigXmlDoc.Load(packageConfigPath);

            var packageTags = packageConfigXmlDoc.GetElementsByTagName("package").Cast<XmlNode>().ToList();
            foreach (var packageTag in packageTags)
            {
                var attributes = packageTag.Attributes.Cast<XmlAttribute>().ToList();
                var idAttribute = attributes.FirstOrDefault(a => a.Name == "id".ToLower());
                var versionAttribute = attributes.FirstOrDefault(a => a.Name == "version".ToLower());

                if (idAttribute != null && versionAttribute != null)
                {
                    var reference = _projectReferences.FirstOrDefault(r => r.Name.ToLower() == idAttribute.Value.ToLower()) as ProjectReference;
                    if (reference != null)
                    {
                        reference.SetVersion(new Version(versionAttribute.Value));
                    }
                }
            }
        }
    }
}
