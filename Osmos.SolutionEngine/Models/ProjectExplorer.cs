﻿using Osmos.SolutionEngine.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace Osmos.SolutionEngine.Models
{
    class ProjectExplorer : IProjectExplorer
    {
        string _projectFilePath;
        string[] _compileFilePaths;
        string[] _contentFilePaths;
        IProjectItem _root;

        public ProjectExplorer(string projectFilePath)
        {
            _projectFilePath = projectFilePath;
            Load();
        }

        public string ProjectFilePath { get { return _projectFilePath; } }
        public string[] CompileFilePaths { get { return _compileFilePaths; } }
        public string[] ContentFilePaths { get { return _contentFilePaths; } }
        public IProjectItem Root { get { return _root; } }

        private string[] _LoadFiles(string tagName)
        {
            var csProjXmlDoc = new XmlDocument();
            csProjXmlDoc.Load(_projectFilePath);

            var tags = csProjXmlDoc.GetElementsByTagName(tagName).Cast<XmlNode>().ToList();
            var tmp = new List<string>();
            foreach (var tag in tags)
            {
                var attributes = tag.Attributes.Cast<XmlAttribute>().ToList();
                var includeAttribute = attributes.FirstOrDefault(a => a.Name.ToLower() == "include");
                if (includeAttribute != null)
                {
                    string value = includeAttribute.Value;
                    tmp.Add(value);
                }
            }
            return tmp.ToArray();
        }

        public void Load() {
            _compileFilePaths = _LoadFiles("Compile");
            _contentFilePaths = _LoadFiles("Content");
            _root = ProjectItem.GetRoot(this);
        }
    }
}
