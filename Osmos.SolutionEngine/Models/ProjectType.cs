﻿using Osmos.SolutionEngine.Interfaces;
using System;

namespace Osmos.SolutionEngine.Models
{
    class ProjectType : IProjectType
    {
        Guid _id;
        string _name;

        public Guid Id { get { return _id; } }
        public string Name { get { return _name; } }

        public ProjectType(Guid id, string name)
        {
            _id = id;
            _name = name;
        }
    }
}
