﻿
namespace Osmos.SolutionEngine.Exceptions
{
    public class BadSlnFileException : System.Exception
    {
        public BadSlnFileException()
            : base("The solution file is broken.")
        {

        }
    }
}
