﻿
namespace Osmos.SolutionEngine.Exceptions
{
    public class BadProjectIdException : System.Exception
    {
        public BadProjectIdException(string guidStr)
            : base(string.Format("The project id {0} is not good.", guidStr))
        {

        }
    }
}
